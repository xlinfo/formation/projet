create database if not exists users;
#create user if not exists jerome@localhost identified by "jerome";
#grant all privileges on users.* to jerome@localhost;
#flush privileges;

CREATE TABLE if not exists users.users (
	  `id` int(11) NOT NULL,
	  `utilisateur` varchar(20) NOT NULL,
	  `date_abonnement` date NOT NULL,
	  `date_resiliation` date DEFAULT NULL
);

ALTER TABLE users.users ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `utilisateur` (`utilisateur`);
ALTER TABLE users.users MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE VIEW if not exists users.abonnements  AS  select count(`id`) AS `tous`,(count(`id`) - count(`date_resiliation`)) AS `actifs`,count(`date_resiliation`) AS `resilies` from users.users ;
