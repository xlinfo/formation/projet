# Projet

Vous êtes porteur d’un projet de plateforme wordpress.
Avec comme cahier des charges :

* 1 quota de 1 Go de données par utilisateur (sur une partition /home séparée)
* 1 sous-domaine par utilisateur en « utilisateur.votre_prenom.lan » qui pointe sur le site wordpress préconfiguré de l’utilisateur
* Une limite d’upload de 10M par fichier téléchargé.
* La possibilité d’envois de mails à partir du serveur (pour les scripts d’administration et les sites web)
* un accès FTP par utilisateur (avec droit en lecture/écriture)
* un accès sécurisé par SSL à la base de données via phpmyadmin
* une adresse email « utilisateur@votre_prenom.lan »
* une garantie de sauvegarde quotidienne et une restauration sur demande qui court sur le dernier mois.
* Vous devrez en outre tenir à jour une base de données recensant vos utilisateurs, avec leur date d’inscription et de résiliation, ainsi qu'une une vue recensant le nombre d’inscrits du jour, le nombre d’inscrits depuis le début, ainsi que celui des résiliés, que vous enverrez chaque jour à l’administrateur.

Vous devez pour cela préparer un serveur de test (sous debian) que vous rajouterez à votre domaine votre_prenom.lan :
Et automatiser le tout pour la mise en production en trois étapes :
* L’installation du serveur lui-même avec procédures, et playbooks ansible le cas échéant.
* La création d’utilisateurs avec envoi d’un mail avec leurs identifiants ftp et phpmyadmin et le lien vers leur site wordpress.
* La sauvegarde et la restauration des sites, ainsi que la suppression des sites (et noms de domaines) des utilisateurs résiliés.

Et enfin, via votre dépôt gitlab, présenter votre projet en vous servant des tableaux kanban (tickets) et en joignant scripts, playbooks et tous élément que vous jugerez utiles… Tout en expliquant vos choix (README)

**Mots clés :**   
apache user_dir  
php.ini  
wordpress: wp-config.php  
ansible playbooks  
setgid  
umask  
/etc/skell  
journaled quotas  
pwgen  
ldapscripts  
nsupdate  

##  Installation machine virtuelle  

* [x]  Installation debian 9
* [x]  Partitionnement manuel (8G /, 20G /home, 2G swap)

## Préparation serveur

* [x]  Mettre à jour les packages
* [x]  Installer vim, resolvconf, quota
* [x]  Fixer adresse ip
* [x]  ajouter champs A dans zone DNS sur debian (NS)
* [x]  Ajouter clef ssh au compte root
* [x]  Activer les quotas sur la partition /home dans fstab

*Pour les quotas, après avoir mis à jour /etc/fstab :  
mount -o remount /home  
quotacheck -ugm /home && quotaon /home*

## Installation des services

* [x]  playbook exim4.yml
* [x]  playbook lamp.yml : apache, mariadb, phpmyadmin, proftpd
* [x]  playbook ldap.yml : libnss-ldapd, ldapscripts

*Pour proteger secret.yml qui contient le mot de passe admin de ldap...  
echo passwddecryptage >  /root/.vault_passwd  
chmod 600 /root/.vault_passwd  
ansible-vault encrypt vars/secret.yml --vault-password-file /root/.vault_passwd  
ansible-playbook ldap.yml --vault-password-file /root/.vault_passwd*


## Scripts d'administration

* [x]  users.sql
* [x]  install_wordpress
* [x]  remove_wordpress
* [x]  sauvegarde_sites
* [x]  restauration_site
* [x]  stats
* [x]  playbook install_scripts.yml

*Pour users.sql :  
Créer un utilisateur et lui donner tous les privilèges sur la base de données le temps de créer le script via phpmyadmin (export)*

## Playbooks d'administration

* [x]  adduser.yml
* [x]  adduser_debian.yml
* [x]  adduser_projet.yml
* [x]  deluser.yml
* [x]  deluser_debian.yml
* [x]  deluser projet.yml
* [x]  restaure_site.yml

*Les playbooks adduser_debian, adduser_projet comme deluser_debian et deluser_projet ne s'appellent pas directement mais sont appelés respectivement par : adduser.yml et deluser.yml.  
ansible-playbook adduser.yml -e "user=toto"  
ansible-playbook deluser.yml -e "user=toto"  
ansible-playbook restaure_site.yml -e "user=toto" -e "date=yyyy-mm-dd"*  